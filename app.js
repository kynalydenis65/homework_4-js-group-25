// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

// let num1 = parseFloat(prompt('Будь ласка, введіть перше число!'));
// let num2 = parseFloat(prompt('Будь ласка, введіть друге число!'));

// let operation = prompt('Введіть математичну операцію (+, -, *, /)');

// function calcSum(num1, num2, operator) {
//     switch (operator) {
        
//         case '+':
//             return num1 + num2;
        
//             case '-':
//             return num1 - num2;
        
//             case '*':
//             return num1 * num2;
        
//             case '/':
//             if (num2 !== 0) {
//                 return num1 / num2;
//             } else {
//                 console.error('Ділення на нуль неможливе');
//                 return undefined;
//             }
//         default:
//             console.error('Введено невірну операцію');
//             return undefined;
//     }
// }

// if (!isNaN(num1) && !isNaN(num2)) {
//     // Викликаємо функцію calculate та виводимо результат у консоль
//     let result = calcSum(num1, num2, operation);
//     if (result !== undefined) {
//         console.log(`Результат: ${result}`);
//     }
// } else {
//     console.error('Введіть коректні числа');
// }



do {
    let num1 = parseFloat(prompt('Будь ласка, введіть перше число!'));
    let num2 = parseFloat(prompt('Будь ласка, введіть друге число!'));
  
    let operation = prompt('Введіть математичну операцію (+, -, *, /)');
  
    function calcSum(num1, num2, operator) {
      switch (operator) {
        case '+':
          return num1 + num2;
        case '-':
          return num1 - num2;
        case '*':
          return num1 * num2;
        case '/':
          if (num2 !== 0) {
            return num1 / num2;
          } else {
            console.error('Ділення на нуль неможливе');
            return undefined;
          }
        default:
          console.error('Введено невірну операцію');
          return undefined;
      }
    }
  
    if (!isNaN(num1) && !isNaN(num2)) {
      let result = calcSum(num1, num2, operation);
      if (result !== undefined) {
        console.log(`Результат: ${result}`);
      }
    } else {
      console.error('Введіть коректні числа');
    }
  
    // Питаємо користувача, чи він хоче продовжити обчислення
    let continueCalculation = confirm('Бажаєте продовжити обчислення?');
  } while (continueCalculation);
  